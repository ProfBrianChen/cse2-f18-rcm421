//Ryan McGuiness
//CSE-002-210
//December 4, 2018
import java.util.Scanner;
public class TicTacToe{
  public static void main(String [] args){
    int row = 3;//row size
    int col = 3;//column size
    boolean isFull = false;//sets initial value of full to false
    boolean haveWinner = false;//sets winner to false
    boolean validInput = false;//used to validate input
    char [][] gameBoard = new char [row] [col];//game board of a 2-D array
    char currentPlayer = 'O';//Player that starts match is O
    int rowInput;//user input of row variable
    int colInput;//user input of column variable
    initBoard(gameBoard);// initializes '-' to the blank board
    Scanner scnr = new Scanner(System.in);
    do{
      printBoard(gameBoard);//prints grid so users can see
      do{
       System.out.print("Player " + currentPlayer + " enter your row[0-2]: ");//prompts user
       if(!scnr.hasNextInt()){
        scnr.next(); // checks if row that user entered is an integer
       System.out.println("ERROR: Restart turn, please enter integers only");//prints error statement if row value isnt an integer
       continue;//starts loop over if there is an error
       }
       rowInput = scnr.nextInt();//saves user value to variable if it is a integer
       System.out.print("Now enter column[0-2]:");//prompts user to enter column
       if(!scnr.hasNextInt()){
        scnr.next();// checks if column that user entered is an integer
       System.out.println("ERROR: Restart turn, please enter integers only");//prints error statement if row value isnt an integer
       continue;//starts loop over if there is an error
       }
       colInput = scnr.nextInt();//saves user value to variable if it is an integer
       validInput = placeMarker(gameBoard, rowInput, colInput, currentPlayer);//places marker
       if(validInput == false){
         System.out.println("ERROR: Invalid entry");
       }
      }while(validInput == false);
      isFull = checkFull(gameBoard);//checks if board is full 
      haveWinner = checkWin(gameBoard);//checks if there is a winner
      if(haveWinner == false){
      currentPlayer = switchPlayer(currentPlayer);//if there is a winner it does not switch current player
      }
    }while(haveWinner == false && isFull == false);//loop continues if there is no winner and board isnt full
    printBoard(gameBoard);//prints winning board
    //if there is a winner prints out winner and if there is no winner it is a draw
    if(haveWinner == true){
      System.out.println("Player " + currentPlayer + " has won");
    }else{
      System.out.println("This game has ended in draw.");
    }
  }
public static void initBoard(char [][] gameBoard){
  //sets whole board to blank
  for(int i = 0; i < gameBoard[0].length; i++){
    for(int j = 0; j < gameBoard.length; j++){
      gameBoard[i][j] = '-';
    }
    }
}
public static void printBoard(char[][] gameBoard){
    int i;  
    int j;
    int counter = 0;
    System.out.println("   0   1   2  ");//Gives the user columns to choose from
    System.out.println("--------------");// prints top boarder
    for(i = 0; i < gameBoard[0].length; i++){
      //prints out row numbers  
      System.out.print(counter + "| ");
      for(j = 0; j < gameBoard.length; j++){
       //prints each value saved in game board
        System.out.print(gameBoard[i][j] + " | ");
        }
      System.out.println();
      System.out.println("--------------");//prints bottom of row
      counter++;
    }
      
    }
public static char switchPlayer(char currentPlayer){
  //switches current player to other player
  if(currentPlayer == 'X'){
    currentPlayer = 'O';
  }else if(currentPlayer == 'O'){
    currentPlayer = 'X';
  }
  return currentPlayer;
}
public static boolean checkFull(char [][] gameBoard){
  int i;
  int j;
  boolean isFull = true;
  for(i = 0; i < gameBoard.length; i++){
    for(j = 0; j < gameBoard[0].length; j++){
      if(gameBoard[i][j] == '-'){
        isFull = false;// if there are any '-' than the board is not full
      }  
    }
  }
  return isFull;
}
public static boolean placeMarker(char[][]gameBoard, int rowInput, int colInput, char currentPlayer){
 boolean validInput = false; //starts off as false
  //if both of users inputs fit parameters returns true if not returns false
 if((rowInput >= 0)  && (rowInput <= 2)){
    if((colInput >= 0) && (colInput <= 2)){
      if(gameBoard[rowInput][colInput] == '-'){
       gameBoard[rowInput][colInput] = currentPlayer;
       validInput = true;
      }
    }else{
      System.out.println("Error: Column variable is out of bounds.");
      validInput = false;
    }
  }else{
    System.out.println("Error: Row variable is out of bounds.");
    validInput = false;
  }
  return validInput;
}


public static boolean checkWin(char[][]gameBoard){
  //have winner is true if any row, column, or diagnal have three of a kind
  boolean haveWinner = (checkRows(gameBoard) || checkCols(gameBoard) || checkDiags(gameBoard));
  return haveWinner;
}
//checks for three of a kind
public static boolean checkThree(char x, char y, char z){
  return ((x != '-') && (x == y) && (y == z));
}
public static boolean checkRows(char [][] gameBoard){
  //goes through each row and runs it through the checkThree method
  for(int i = 0; i < gameBoard.length; i++){
    if(checkThree(gameBoard[i][0], gameBoard[i][1], gameBoard[i][2]) == true){
      return true;
    }
   }
return false;
}
public static boolean checkCols(char[][] gameBoard){
  //goes through each column and runs it through the check three method
  for(int i = 0; i < gameBoard[0].length; i++){
    if(checkThree(gameBoard[0][i], gameBoard[1][i], gameBoard[2][i]) == true){
      return true;
    }
  }
  return false;
}
public static boolean checkDiags(char[][]gameBoard){
  //checks both diagnals for three of a kind
  return((checkThree(gameBoard[0][0], gameBoard[1][1], gameBoard[2][2]) == true) || (checkThree(gameBoard[2][0], gameBoard[1][1], gameBoard[0][2]) == true));
}
}
