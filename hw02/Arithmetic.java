public class Arithmetic {
  
  public static void main(String[] args) {
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of shirts
  int numShirts = 2;
//Cost per Shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants;
double totalCostOfShirts;
double totalCostOfBelts;

//total cost of pants
totalCostOfPants = numPants * pantsPrice;

//total cost of shirts
totalCostOfShirts = numShirts * shirtPrice;

//total cost of belts 
totalCostOfBelts = numBelts * beltCost;  

double salesTaxOnPants;
double salesTaxOnShirts;
double salesTaxOnBelts;

//tax charged on pants
salesTaxOnPants = (int) (totalCostOfPants * paSalesTax*100) / 100.0;

//tax charged on shirts
salesTaxOnShirts = (int) (totalCostOfShirts * paSalesTax * 100) / 100.0;

//tax charged on Belts
salesTaxOnBelts = (int) (totalCostOfBelts * paSalesTax * 100) / 100.0;

double totalCostOfPurchases;

//total cost of purches before tax
totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;

double totalSalesTax;

//sales tax charged on total
totalSalesTax = (int) (totalCostOfPurchases * paSalesTax * 100) / 100.0;

double total;

// total amount charged including tax
total = totalSalesTax + totalCostOfPurchases;
    
    System.out.println("Cost of pants is " + totalCostOfPants);
    System.out.println("Cost of belts is " + totalCostOfBelts);
    System.out.println("Cost of shirts is " + totalCostOfShirts);
    System.out.println("Sales tax on pants is " + salesTaxOnPants);
    System.out.println("Sales tax on belts is " + salesTaxOnBelts);
    System.out.println("Sales tax shirts is " + salesTaxOnShirts);
    System.out.println("Total amount before tax is " + totalCostOfPurchases);
    System.out.println("Total sales tax charged is " + totalSalesTax);
    System.out.println("Total amount spent after tax: " + total);
  }
}