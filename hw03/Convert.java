import java.util.Scanner;
public class Convert{
  public static void main (String [] args){
    //User entered measurements
    Scanner myScanner = new Scanner ( System.in );
    System.out.print ("Enter the affected area in acres: ");
    double areaAcres = myScanner.nextDouble();
    
    System.out.print ("Enter the rainfall in the affected areas: ");
    double rainfall = myScanner.nextDouble ();
    // variables needed for equation
    double gallons;
    double cubicMiles ;
    double cubicFeet;
    double cubicInches;
    double constant = 9.0816859724455;
   //converting to gallons
    cubicFeet = (43560 * areaAcres) / 12.0;
    cubicInches = 1728 * rainfall;
    
    //converting gallons to cubic miles
    gallons = (cubicInches * cubicFeet) / 231.0;
    
    cubicMiles = gallons * (Math.pow(constant, -13.0));
    // printing with proper notation
    System.out.print(cubicMiles + " cubic miles");
  }
}