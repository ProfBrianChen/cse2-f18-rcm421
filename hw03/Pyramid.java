import java.util.Scanner;
public class Pyramid {
  public static void main (String [] args){
    //user entered measurements
    Scanner myScanner = new Scanner ( System.in );
    System.out.print ("Length of square side: ");
    double length = myScanner.nextDouble();
    
    System.out.print ("Height of the pyramid: ");
    double height = myScanner.nextDouble();
    //variable we are searching for
    double volume;
    //equation for volume 
    volume = ((length * 2) * height) / 3;
    //printing for user to read
    System.out.print("The volume inside the pyramid is: " + volume);
  }
}