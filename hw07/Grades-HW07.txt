Grading Sheet for HW7

Compiles    				20 pts
Comments 				10 pts
PrintMenu Method			10 pts
Checks for Invalid Characters	10 pts
getNumOfNonWSCharacters 	10 pts
    -5 pts does not print correct number
getNumOfWords			10 pts
    -5 pts does not print correct number
findText				10 pts
replaceExclamation			10 pts
shortenSpace				10 pts

Total: 90/100