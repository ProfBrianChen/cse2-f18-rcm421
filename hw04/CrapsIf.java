//Ryan McGuiness
//cse-002
//September 26,2018
//This class will create a Craps Game.
import java.util.Scanner;
public class CrapsIf{
  public static void main (String [] args){
    //select if you want random numbers selected for you or there is the option of selecting your own numbers.
    Scanner scnr = new Scanner(System.in);
    System.out.print("If you would like to randomly cast enter 'random.' if you would like to ");
      
      String input = scnr.nextLine();
      String slang = " "; 
      
      int dice1 = 0;
      int dice2 = 0;
    //offers two random numbers or offers the option to enter your own  
    if (input.equals("random.")){
        dice1 = (int) ((Math.random() * 7) + 1);
        dice2 = (int) ((Math.random() * 7) + 1);
      } else if (input.equals("choose")){
      System.out.println("Enter an interger between 1 and 6 to be your first die: ");
      dice1 = scnr.nextInt();
      System.out.println("Enter an interger between 1 and 6 to be your second die: ");
      dice2 = scnr.nextInt();
    }
    
    if (dice1 == 1)//dice1 = 1, dice2 = 1-6
      if (dice2 == 1){
        slang = "Snake Eye";
      } else if(dice2 == 2){
        slang = "Ace Duce";
      } else if(dice2 == 3){
        slang = "Easy Four";
      } else if(dice2 == 4){
        slang = "Easy Five";
      } else if(dice2 == 5){
        slang = "Easy Six";
      } else if(dice2 == 6){
        slang = "Seven Out";
      }
    if (dice1 == 2) //dice1 = 2, dice2 = 1-6
      if(dice2 == 1){
        slang = "Ace Duece";
      } else if(dice2 == 2){
        slang = "Hard Four";
      } else if(dice2 == 3){
        slang = "Fever Five";
      } else if(dice2 == 4){
        slang = "Easy Six";
      } else if(dice2 == 5){
        slang = "Seven Out";
      } else if(dice2 == 6){
        slang = "Easy Eight";
      } 
    if (dice1 == 3)    //dice1 = 3, dice2 = 1-6
      if(dice2 == 1){
        slang = "Easy Four";
      } else if(dice2 == 2){
        slang = "Fever Five";
      } else if(dice2 == 3){
        slang = "Hard Six";
      } else if(dice2 == 4){
        slang = "Seven Out";
      } else if(dice2 == 5){
        slang = "Easy Eight";
      } else if(dice2 == 6){
        slang = "Nine";
      }
    if (dice1 == 4)    //dice1 = 4, dice2 = 1-6
      if(dice2 == 1){
        slang = "Fever Five";
      } else if(dice2 == 2){
        slang = "Easy Six";
      } else if(dice2 == 3){
        slang = "Seven Out";
      } else if(dice2 == 4){
        slang = "Hard Eight";
      } else if(dice2 == 5){
        slang = "Nine";
      } else if(dice2 == 6){
        slang = "Easy Ten";
      }
    if (dice1 == 5)  //dice1 = 5,dice2 = 1-6
      if(dice2 == 1){
        slang = "Easy Six";
      } else if(dice2 == 2){
        slang = "Seven Out";
      } else if(dice2 == 3){
        slang = "Easy Eight";
      } else if(dice2 == 4){
        slang = "Nine";
      } else if(dice2 == 5){
        slang = "Hard Ten";
      } else if(dice2 == 6){
        slang = "Yo-leven";
      }
    if (dice1 == 6)    //dice1 = 6, dice2 = 1-6
      if(dice2 == 1){
        slang = "Seven Out";
      } else if(dice2 == 2){
        slang = "Easy Eight";
      } else if(dice2 == 3){
        slang = "Nine";
      } else if(dice2 == 4){
        slang = "Easy Ten";
      } else if(dice2 == 5){
        slang = "Yo-leven";
      } else if(dice2 == 6){
        slang = "Boxcars";
      }
    //this print the slang term for the two random numbers or the two selected numbers. 
System.out.print(slang);
  
  } 
  
}

