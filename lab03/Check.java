//Ryan McGuiness
//September 12,2018
//
//This program calculates how to split a large number amungst a large group of people.
//It also calculates a percentage of the total and splits that evenly too.It
import java.util.Scanner;
public class Check{
  public static void main (String[] args) {
    Scanner myScanner = new Scanner ( System.in );
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();  
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //we want to conver the percentage to a decimal
     
    System.out.print ("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    totalCost = checkCost * (1 +tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) %10;
    System.out.println("Each Person in the group owes $" + dollars + '.' + dimes + pennies);
  }
}