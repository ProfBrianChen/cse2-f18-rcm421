//Ryan McGuiness
//cse-002
//Nov 13,2018
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 

    public static void main(String[] args) {
 Scanner scan = new Scanner(System.in);
 // suits club, heart, spade or diamond
 String[] suitNames = { "C", "H", "S", "D" };
 String[] rankNames = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
 String[] cards = new String[52];
 String[] hand = new String[5];
 int numCards = 5;
 int again = 1;
 int index = 51;
 for (int i = 0; i < 52; i++) {
     cards[i] = rankNames[i % 13] + suitNames[i / 13];
     System.out.print(cards[i] + " ");
 }
 System.out.println();

 printArray(cards);
 shuffle(cards);
 printArray(cards);
 while (again == 1) {
     hand = getHand(cards, index, numCards);
     printArray(hand);
     index -= numCards;
     System.out.println("Enter a 1 if you want another hand drawn");
     again = scan.nextInt();
 }
    }

public static void printArray(String[] cards) {
 //prints out all values of an array
 for (int i = 0; i < cards.length; i++) {
     System.out.print(cards[i] + " "); 
 }
 System.out.println(); //prints empty line
}

public static String[] shuffle(String[] cards) {
  int count;
  Random randomNumGenerator = new Random();
    //shuffles the cards by taking one in deck and switching it with the top card
  for (count = 0; count < 100; count++) {
     int randomIndex = randomNumGenerator.nextInt(51) + 1;
     
     String temp = cards[0]; //sets top card to temporary value
     cards[0] = cards[randomIndex]; //flips top and bottom cards
     cards[randomIndex] = temp; 
      }
 return cards;
 }

public static String[] getHand(String[] cards, int index, int numCards) {
  //if there arent enough cards in the deck deck is shuffled again then continues
  if(numCards > index){
    shuffle(cards);
    index = 51;
  }
  
  String [] hand = new String [numCards];
  //Deals out number of cards from the top of the deck
 for (int i = 0; i < numCards; i++) {
     hand[i] = cards[index];
       index--;
 }
 return hand;
    }
}
