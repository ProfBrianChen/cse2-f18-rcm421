//Ryan McGuiness 
public class Cyclometer{
  //main method required for every Java program
  public static void main (String[] args) {
int secsTrip1=480; // seconds in trip 1
int secsTrip2=3220; // seconds in trip 2
int countsTrip1=1561; // number of counts for trip1
int countsTrip2=9037; // number of counts for trip 2
//our intermediate variables and output data.
double wheelDiameter=27.0, //
PI=3.14159, // constant 
feetPerMile=5280, //
inchesPerFoot=12, //
secondsPerMinute=60; //
double distanceTrip1, distanceTrip2, totalDistance; //distance travelled during each and both trips


System.out.println("Trip 1 took " + 
   (secsTrip1/secondsPerMinute) + " minutes and had " +
    countsTrip1 + " counts.");
System.out.println("Trip 2 took " + 
   (secsTrip2/secondsPerMinute) + " minutes and had " +
         countsTrip2 + " counts.");
// run the calculations; store the values. Document your
//calculations here. What are you calculationg?
// calculating ammount of trips and counts
//
distanceTrip1=countsTrip1*wheelDiameter*PI;
//Aboce gives distance in inchesPerFoot
//(for each count, a rotation of the wheel travels 
// the diameter in inches times PI)
distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distance in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
totalDistance=distanceTrip1+distanceTrip2;

//Print out the output data.
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");



  } // ed of main method
}//end of class

