//Ryan McGuiness
//cse-002
import java.util.Random;
public class Arrays{
  public static void main(String[] args){
   //creates random number
    Random rand = new Random();
    //creates two arrays with index's of 0-99
    int [] arrayOneHundred = new int[100];
    int [] arrayCount = new int[100];
    int i;
    //this loop increments and saves a random number to each index in arrayOneHundred
    for(i = 0; i < 100; i++){
      int rando=rand.nextInt(100);
      arrayOneHundred [i] = rando;
      /*this adds one to each index corresponding with the random number.
       * Which is just keeping track how many times each number is repeated
       */
      arrayCount[rando]+=1;
      System.out.print(arrayOneHundred[i] + ", ");
    }
    //Goes through each index of arrayCount and prints the amount of times each number is repeated.
    for(i=0; i < 100; i++){
    System.out.println(i + " occurs "+ arrayCount[i]+ " times." );
    }
  }
}

