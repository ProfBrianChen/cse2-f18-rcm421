//Ryan McGuiness
//cse-002
//September 18,2018
import java.util.Random;
public class CardGenerator {
  public static void main(String [] args) {
    //This program will randomly select a card from a deck
    Random rand = new Random();
    int randomCard = rand.nextInt(52) + 1; //selects a random number 1-52
    String suit;
   //depending on number it selects a suit     
 if (randomCard >= 39) {
   suit = "Spades";
 } else if (randomCard >= 26) {
   suit = "Hearts";
 } else if (randomCard >= 13) {
   suit = "Clubs";
 } else {
   suit = "Diamonds";
 }

int cardNumber;
    cardNumber = randomCard % 13;
String number;
 // depending on number 1-13 it will state its face
switch (cardNumber) {
   case 1:
      number = "Ace";
        break;
   case 2:
      number= "2";
        break;
   case 3:
      number = "3";
        break;
   case 4:
      number = "4";
        break;
   case 5:
      number = "5";
        break;
   case 6:
      number = "6";
        break;
   case 7:
      number = "7";
        break;
   case 8:
      number = "8";
        break;
   case 9:
      number = "9";
        break;
   case 10:
      number = "10";
        break;
   case 11:
      number = "Jack";
        break;
   case 12:
      number = "Queen";
        break;
   case 13:
      number = "King";
        break;
   default:
      number = "Wild Card";
    }
 
   
    System.out.println(number + " of " + suit);
  }
}